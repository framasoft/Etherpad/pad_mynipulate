[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# PAD\_MYNIPULATE

At [Framasoft](https://framasoft.org), we have a [mypads](https://framagit.org/framasoft/ep_mypads/), and, sometimes, we need to manipulate a bit the database.
So here's a tool to help us doing this.

**NB:** As we use PostgreSQL as our etherpad's databases, the scripts works only with PostgreSQL.

## mynipulate

`mynipulate` asks you for the database name if you don't provide it as argument on the command line.

# Installation

Have a look at the [INSTALL.md file](INSTALL.md)

# License

GNU General Public License, version 3. Check the [LICENSE file](LICENSE).
