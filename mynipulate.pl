#!/usr/bin/perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -strict;

use Config::YAML;
use Mojo::Pg;
use Mojo::JSON qw(decode_json encode_json);
use Mojo::Util qw(decode encode);
use Term::ReadLine;
use Text::Slugify qw(slugify);
use File::Spec::Functions;
use File::Path qw/make_path/;
use Data::Dumper;

binmode(STDOUT, ":utf8");
binmode(STDIN, ":utf8");

my $c = Config::YAML->new(config => '/etc/mynipulate.yml');
my @bases = sort keys %{$c->get_bases};

##################
# Choose database
##################
my $base;

if (scalar(@bases) == 1) {
    $base = $bases[0];
} elsif (defined $ARGV[1] && _is_valid_base($ARGV[1], 'name')) {
    $base = $ARGV[1];
} else {
    _print_bases();
    $base = _choose_base();
}

## Connect to database
my $addr  = 'postgresql://';
$addr    .= $c->get_bases->{$base}->{user};
$addr    .= '@'.$c->get_host;
$addr    .= ':'.$c->get_port;
$addr    .= '/'.$base;

my $pg = Mojo::Pg->new($addr);
$pg->password($c->get_bases->{$base}->{pwd});

my $db = $pg->db;

#################
# Create console
#################
print <<EOF;
Welcome on mynipulate.
Type 'help' to get some help, 'exit' to exit. Easy, isn't it ?
EOF

my $term = Term::ReadLine->new('mynipulate');
if (my $attr = $term->Attribs) {
    $attr->{completion_function} = \&_complete_word;
}

##############
# Get history
##############
my $conf_dir     = (defined $ENV{XDG_CONFIG_HOME}) ? catdir($ENV{XDG_CONFIG_HOME}, 'mynipulate') : catdir($ENV{HOME}, '.config', 'mynipulate');
make_path $conf_dir unless (-d $conf_dir);

my $history_file = catfile($conf_dir, 'history');
my @history;

if (-f $history_file) {
    open my $hist, '<', $history_file or die "Unable to open $history_file: $!";
    while (defined(my $line = <$hist>)) {
        chomp $line;
        _addtohistory($line);
        $term->addhistory($line);
    }
    close $hist;
}

#####################
# Let the game begin
#####################
my $prompt = $base.' $ ';

# For autocomplete
my @words = qw(exit          help        connect
               count_groups  count_pads  count_users
               search_group  search_pad  search_user
               infos_group   infos_pad   infos_user
               attach_pad_to_group
               attach_user_to_group
               attach_user_to_group_as_admin
               detach_user_from_group
               detach_admin_from_group
               modify_user   empty_global_author_token
              );

my @commands;
while (defined($_ = $term->readline($prompt))) {
    chomp;
    @commands   = split(' ', $_);
    my $command = shift @commands || '';
    _addtohistory($_) unless ($command eq 'exit');

         if ($command eq 'exit') {
        _exit();
    } elsif ($command eq 'help') {
        _help();
    } elsif ($command eq 'count_groups') {
        _count_groups();
    } elsif ($command eq 'count_pads') {
        _count_pads();
    } elsif ($command eq 'count_users') {
        _count_users();
    } elsif ($command eq 'search_group') {
        _search_group();
    } elsif ($command eq 'search_pad') {
        _search_pad();
    } elsif ($command eq 'search_user') {
        _search_user();
    } elsif ($command eq 'infos_group') {
        _infos_group()
    } elsif ($command eq 'infos_pad') {
        _infos_pad()
    } elsif ($command eq 'infos_user') {
        _infos_user()
    } elsif ($command eq 'attach_pad_to_group') {
        _attach_pad_to_group()
    } elsif ($command eq 'attach_user_to_group') {
        _attach_user_to_group()
    } elsif ($command eq 'attach_user_to_group_as_admin') {
        _attach_user_to_group_as_admin()
    } elsif ($command eq 'detach_user_from_group') {
        _detach_user_from_group()
    } elsif ($command eq 'detach_admin_from_group') {
        _detach_admin_from_group()
    } elsif ($command eq 'modify_user') {
        _modify_user()
    } elsif ($command eq 'empty_global_author_token') {
        _empty_global_author_token()
    } elsif ($command eq 'connect') {
        _connect()
    } else {
        print 'WARNING: Unknown command', "\n";
        _help();
    }
}

sub _help {
    print <<EOF
Available commands:
  - help                                         : print this message
  - exit                                         : exit program
  - count_groups                                 : print the number of existing mypads groups
  - count_pads                                   : print the number of existing mypads pads
  - count_users                                  : print the number of existing mypads users
  - search_group <group>                         : search a group matching the given string (which is sluggified)
  - search_pad <pad>                             : search a pad matching the given string (which is sluggified)
  - search_user <email>                          : search a user with that email
  - infos_group <group>                          : print informations about this group
  - infos_pad <pad>                              : print informations about this pad
  - infos_user <user>                            : print informations about this user
  - attach_pad_to_group <pad> <group> [detach]   : attach a pad to a group
                                                   If you add "detach" at the end of the command,
                                                   it will detach the pad from its previous group first
  - attach_user_to_group <user> <group>          : attach a user to a group
  - attach_user_to_group_as_admin <user> <group> : attach a user to a group as admin of the group
  - detach_user_from_group <user> <group>        : detach a user from a group
  - detach_admin_from_group <user> <group>       : detach a admin from a group
  - modify_user <user> <new_login> <new_email>   : modify login and email address of an user
  - empty_global_author_token <user>             : remove the etherpad’s globalAuthor token attached to the user
  - connect <database>                           : change the database you're working on
EOF
}

print "\n";
_exit();
#####################
# Internal functions
#####################

sub _count_groups {
    my $c = shift @commands;
    say sprintf("number of groups: %d", ($db->query("SELECT count(key) FROM store WHERE key LIKE 'mypads:group:%';")->hashes->first->{count}));
}
sub _count_pads {
    my $c = shift @commands;
    say sprintf("number of pads: %d", ($db->query("SELECT count(key) FROM store WHERE key LIKE 'mypads:pad:%';")->hashes->first->{count}));
}
sub _count_users {
    my $c = shift @commands;
    say sprintf("number of users: %d", ($db->query("SELECT count(key) FROM store WHERE key LIKE 'mypads:user:%';")->hashes->first->{count}));
}
sub _search_group {
    my $c = shift @commands;
    my $r = $db->query("SELECT key FROM store WHERE key LIKE ?;", ('mypads:group:'.slugify($c).'-%'));
    if ($r->rows) {
        say "Here are the results of the search:";
        $r->hashes->each(sub {
            my ($e, $num) = @_;
            $e->{key} =~ s/^mypads:group://;
            say sprintf("- %s", $e->{key});
        });
    } else {
        say "Sorry, no group found.";
    }
}
sub _search_pad {
    my $c = shift @commands;
    my $r = $db->query("SELECT key FROM store WHERE key LIKE ?;", ('mypads:pad:'.slugify($c).'-%'));
    if ($r->rows) {
        say "Here are the results of the search:";
        $r->hashes->each(sub {
            my ($e, $num) = @_;
            $e->{key} =~ s/^mypads:pad://;
            say sprintf("- %s", $e->{key});
        });
    } else {
        say "Sorry, no pad found.";
    }
}
sub _search_user {
    my $c = shift @commands;
    my $r = $db->query("SELECT key FROM store WHERE key LIKE 'mypads:user:%' AND value::jsonb->>'email' = ?;", ($c));
    if ($r->rows) {
        say "Here are the results of the search:";
        $r->hashes->each(sub {
            my ($e, $num) = @_;
            $e->{key} =~ s/^mypads:user://;
            say sprintf("- %s", $e->{key});
        });
    } else {
        say "Sorry, no user found.";
    }
}
sub _infos_group {
    my $c = shift @commands;
    my $r = $db->query("SELECT value FROM store WHERE key = ?;", ('mypads:group:'.$c));
    if ($r->rows) {
        my $json = decode_json(encode('UTF-8', $r->hashes->first->{value}));
        say "Group id:          ".$c;
        say "Group name:        ".$json->{name};
        say "Group description: ".$json->{description} if $json->{description};
        say "Group admins:" if scalar(@{$json->{admins}});
        for my $i (sort {$a cmp $b} @{$json->{admins}}) {
            say "- $i";
        }
        say "Group users:"  if scalar(@{$json->{users}});
        for my $i (sort {$a cmp $b} @{$json->{users}}) {
            say "- $i";
        }
        say "Group pads:"   if scalar(@{$json->{pads}});
        for my $i (sort {$a cmp $b} @{$json->{pads}}) {
            say "- $i";
        }
    } else {
        say "Sorry, unable to find the group.";
    }
}
sub _infos_pad {
    my $c = shift @commands;
    my $r = $db->query("SELECT value FROM store WHERE key = ?", ('mypads:pad:'.$c));
    if ($r->rows) {
        my $json = decode_json(encode('UTF-8', $r->hashes->first->{value}));
        say "Pad id:    ".$c;
        say "Pad name:  ".$json->{name};
        say "Pad group: ".$json->{group} if $json->{group};
        say "Pad users: " if scalar(@{$json->{users}});
        for my $i (sort {$a cmp $b} @{$json->{users}}) {
            say "- $i";
        }
    } else {
        say "Sorry, unable to find the pad.";
    }
}
sub _infos_user {
    my $c = shift @commands;
    my $r = $db->query("SELECT value FROM store WHERE key = ?;", ('mypads:user:'.$c));
    if ($r->rows) {
        my $json = decode_json(encode('UTF-8', $r->hashes->first->{value}));
        my $global_author_name;
        if ($json->{eplAuthorToken}) {
            my $s2 = $db->query("SELECT value FROM store WHERE key = ?", ('token2author:'.$json->{eplAuthorToken}));
            if ($s2->rows) {
                my $json2 = decode_json(encode('UTF-8', $s2->hashes->first->{value}));
                my $s3 = $db->query("SELECT value FROM store WHERE key = ?", ('globalAuthor:'.$json2));
                if ($s3->rows) {
                    my $json3 = decode_json(encode('UTF-8', $s3->hashes->first->{value}));
                    $global_author_name = $json3->{name};
                }
            }
        }
        say "User id:             ".$c;
        say "User login:          ".$json->{login};
        say "User name:           ".$json->{firstname}." ".$json->{lastname};
        say "User email:          ".$json->{email};
        say "User organization:   ".$json->{organization} if $json->{organization};
        say "User lang:           ".$json->{lang}         if $json->{lang};
        if ($global_author_name) {
            say sprintf("User eplAuthorToken: %s, which globalAuthor’s name is %s", ($json->{eplAuthorToken}, $global_author_name));
        } elsif ($json->{eplAuthorToken}) {
            say sprintf("User eplAuthorToken: %s (unable to find a globalAuthor for that token)", $json->{eplAuthorToken});
        } else {
            say "User eplAuthorToken: None (eplAuthorToken is null or undefined)"
        }
        say "User groups:" if scalar(@{$json->{groups}});
        for my $i (sort {$a cmp $b} @{$json->{groups}}) {
            say "- $i";
        }
    } else {
        say "Sorry, unable to find the user.";
    }
}
sub _attach_pad_to_group {
    my $pad    = shift @commands;
    my $group  = shift @commands;
    my $detach = shift @commands;
    my $detach_first = (defined($detach) && $detach eq 'detach');

    my $s = $db->query("SELECT value FROM store WHERE key = ?", ('mypads:pad:'.$pad));
    if ($s->rows) {
        my $r = $db->query("SELECT value FROM store WHERE key = ?;", ('mypads:group:'.$group));
        if ($r->rows) {
            my $json  = decode_json(encode('UTF-8', $r->hashes->first->{value}));
            my $found = 0;
            for my $u (@{$json->{pads}}) {
                $found = 1 if ($u eq $pad);
            }
            if ($found) {
                say "The pad is already attached to that group.";
            } else {
                my $json2     = decode_json(encode('UTF-8', $s->hashes->first->{value}));
                my $confirmed = 0;
                if ($json2->{group} && !$detach_first) {
                    say sprintf("The pad is already attached to the group %s", ($json2->{group}));
                    say sprintf("Are you sure you want to attach it to the group %s? [y/N] ", ($group));
                    $confirmed = _parse_confirm(_get_input());
                } else {
                    $confirmed = 1;
                }

                if ($confirmed) {
                    if ($json2->{group} && $detach_first) {
                        my $t = $db->query("SELECT value FROM store WHERE key = ?;", ('mypads:group:'.$json2->{group}));
                        if ($t->rows) {
                            my $json3 = decode_json(encode('UTF-8', $t->hashes->first->{value}));
                            my $found = 0;
                            my $new_json3_pads;
                            for my $u (@{$json3->{pads}}) {
                                push @{$new_json3_pads}, $u unless ($u eq $pad);
                            }
                            $json3->{pads} = $new_json3_pads;
                            $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json3), 'mypads:group:'.$json2->{group}));
                            say sprintf("%s removed from group %s.", ($pad, $json2->{group}));
                        } else {
                            say sprintf("Unable to find group %s.", ($json2->{group}));
                        }
                    }
                    $json2->{group} = $group;
                    $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json2), 'mypads:pad:'.$pad));
                    say sprintf("%s updated with group %s", ($pad, $group));

                    push @{$json->{pads}}, $pad;
                    $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json), 'mypads:group:'.$group));
                    say sprintf("%s added to group %s", ($pad, $group));
                } else {
                    say "Aborting the modification."
                }
            }
        } else {
            say "Sorry, the group doesn't exist.";
        }
    } else {
        say "Sorry, the pad doesn't exist.";
    }
}

sub _attach_user_to_group {
    my $user  = shift @commands;
    my $group = shift @commands;

    my $s = $db->query("SELECT value FROM store WHERE key = ?", ('mypads:user:'.$user));
    if ($s->rows) {
        my $r = $db->query("SELECT value FROM store WHERE key = ?;", ('mypads:group:'.$group));
        if ($r->rows) {
            my $json  = decode_json(encode('UTF-8', $r->hashes->first->{value}));
            my ($ufound, $afound) = (0, 0);
            for my $u (@{$json->{users}}) {
                $ufound = 1 if ($u eq $user);
            }
            for my $u (@{$json->{admins}}) {
                $afound = 1 if ($u eq $user);
            }
            if ($ufound) {
                say "The user is already attached to that group as user.";
            } elsif ($afound) {
                say "The user is already attached to that group as admin.";
            } else {
                my $json2     = decode_json(encode('UTF-8', $s->hashes->first->{value}));
                my ($gfound, $confirmed) = (0, 0);
                for my $u (@{$json2->{groups}}) {
                    $gfound = 1 if ($u eq $group);
                }
                if ($gfound) {
                    say sprintf("The user is already attached to the group %s in its record", ($group));
                    say sprintf("Are you sure you want to attach it to the group %s? [y/N] ", ($group));
                    $confirmed = _parse_confirm(_get_input());
                } else {
                    $confirmed = 1;
                }

                if ($confirmed) {
                    unless ($gfound) {
                        push @{$json2->{groups}}, $group;
                        $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json2), 'mypads:user:'.$user));
                        say sprintf("%s updated with group %s", ($user, $group));
                    }

                    push @{$json->{users}}, $user;
                    $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json), 'mypads:group:'.$group));
                    say sprintf("%s added to group %s", ($user, $group));
                } else {
                    say "Aborting the modification."
                }
            }
        } else {
            say "Sorry, the group doesn't exist.";
        }
    } else {
        say "Sorry, the user doesn't exist.";
    }
}

sub _attach_user_to_group_as_admin {
    my $user  = shift @commands;
    my $group = shift @commands;

    my $s = $db->query("SELECT value FROM store WHERE key = ?", ('mypads:user:'.$user));
    if ($s->rows) {
        my $r = $db->query("SELECT value FROM store WHERE key = ?;", ('mypads:group:'.$group));
        if ($r->rows) {
            my $json              = decode_json(encode('UTF-8', $r->hashes->first->{value}));
            my ($ufound, $afound) = (0, 0);
            for my $u (@{$json->{users}}) {
                $ufound = 1 if ($u eq $user);
            }
            for my $u (@{$json->{admins}}) {
                $afound = 1 if ($u eq $user);
            }
            if ($ufound) {
                say "The user is already attached to that group as user.";
            } elsif ($afound) {
                say "The user is already attached to that group as admin.";
            } else {
                my $json2                = decode_json(encode('UTF-8', $s->hashes->first->{value}));
                my ($gfound, $confirmed) = (0, 0);
                for my $u (@{$json2->{groups}}) {
                    $gfound = 1 if ($u eq $group);
                }
                if ($gfound) {
                    say sprintf("The user is already attached to the group %s in its record.", ($group));
                    say sprintf("Are you sure you want to attach it to the group %s? [y/N] ", ($group));
                    $confirmed = _parse_confirm(_get_input());
                } else {
                    $confirmed = 1;
                }

                if ($confirmed) {
                    unless ($gfound) {
                        push @{$json2->{groups}}, $group;
                        $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json2), 'mypads:user:'.$user));
                        say sprintf("%s updated with group %s", ($user, $group));
                    }

                    push @{$json->{admins}}, $user;
                    $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json), 'mypads:group:'.$group));
                    say sprintf("%s added to group %s", ($user, $group));
                } else {
                    say "Aborting the modification."
                }
            }
        } else {
            say "Sorry, the group doesn't exist.";
        }
    } else {
        say "Sorry, the user doesn't exist.";
    }
}

sub _detach_user_from_group {
    my $user  = shift @commands;
    my $group = shift @commands;

    my $s = $db->query("SELECT value FROM store WHERE key = ?", ('mypads:user:'.$user));
    if ($s->rows) {
        my $r = $db->query("SELECT value FROM store WHERE key = ?;", ('mypads:group:'.$group));
        if ($r->rows) {
            my $json = decode_json(encode('UTF-8', $r->hashes->first->{value}));

            my ($ufound, $afound) = (0, 0);
            my @utmp = ();
            for my $u (@{$json->{users}}) {
                if ($u eq $user) {
                    $ufound = 1;
                } else {
                    push @utmp, $u;
                }
            }
            for my $u (@{$json->{admins}}) {
                $afound = 1 if ($u eq $user);
            }
            if (!$ufound) {
                say sprintf("The user is not in that group. To see the users of the group, do:\ninfos_group %s", $group);
            } elsif ($afound) {
                say sprintf("The user is attached to that group as admin. Please use\ndetach_admin_from_group %s %s", ($user, $group));
            } else {
                my $json2 = decode_json(encode('UTF-8', $s->hashes->first->{value}));

                my ($gfound, $confirmed) = (0, 0);
                my @gtmp = ();
                for my $u (@{$json2->{groups}}) {
                    if ($u eq $group) {
                        $gfound = 1;
                    } else {
                        push @gtmp, $u;
                    }
                }
                if (!$gfound) {
                    say sprintf("The user is already detached from the group %s in its record", ($group));
                    say sprintf("Are you sure you want to detach it from the group %s? [y/N] ", ($group));
                    $confirmed = _parse_confirm(_get_input());
                } else {
                    $confirmed = 1;
                }

                if ($confirmed) {
                    if ($gfound) {
                        $json2->{groups} = \@gtmp;
                        $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json2), 'mypads:user:'.$user));
                        say sprintf("%s updated without group %s", ($user, $group));
                    }

                    $json->{users} = \@utmp;
                    $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json), 'mypads:group:'.$group));
                    say sprintf("%s removed from group %s", ($user, $group));
                } else {
                    say "Aborting the modification."
                }
            }
        } else {
            say "Sorry, the group doesn't exist.";
        }
    } else {
        say "Sorry, the user doesn't exist.";
    }
}

sub _detach_admin_from_group {
    my $user  = shift @commands;
    my $group = shift @commands;

    my $s = $db->query("SELECT value FROM store WHERE key = ?", ('mypads:user:'.$user));
    if ($s->rows) {
        my $r = $db->query("SELECT value FROM store WHERE key = ?;", ('mypads:group:'.$group));
        if ($r->rows) {
            my $json = decode_json(encode('UTF-8', $r->hashes->first->{value}));

            my ($ufound, $afound) = (0, 0);
            my @atmp = ();
            for my $u (@{$json->{users}}) {
                $ufound = 1 if ($u eq $user);
            }
            for my $u (@{$json->{admins}}) {
                if ($u eq $user) {
                    $afound = 1;
                } else {
                    push @atmp, $u;
                }
            }
            if ($ufound) {
                say sprintf("The user is attached to that group as user. Please use\ndetach_user_from_group %s %s", ($user, $group));
            } elsif (!$afound) {
                say sprintf("The user is not an admin of that group. To see the admins of the group, do:\ninfos_group %s", $group);;
            } elsif (scalar(@atmp) == 0) {
                say "The user is is the only admin of that group. Aborting the modification.";
            } else {
                my $json2 = decode_json(encode('UTF-8', $s->hashes->first->{value}));

                my ($gfound, $confirmed) = (0, 0);
                my @gtmp = ();
                for my $u (@{$json2->{groups}}) {
                    if ($u eq $group) {
                        $gfound = 1;
                    } else {
                        push @gtmp, $u;
                    }
                }
                if (!$gfound) {
                    say sprintf("The user is already detached from the group %s in its record.", ($group));
                    say sprintf("Are you sure you want to detach it from the group %s? [y/N] ", ($group));
                    $confirmed = _parse_confirm(_get_input());
                } else {
                    $confirmed = 1;
                }

                if ($confirmed) {
                    if ($gfound) {
                        $json2->{groups} = \@gtmp;
                        $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json2), 'mypads:user:'.$user));
                        say sprintf("%s updated without group %s", ($user, $group));
                    }

                    $json->{admins} = \@atmp;
                    $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json), 'mypads:group:'.$group));
                    say sprintf("%s removed from group %s", ($user, $group));
                } else {
                    say "Aborting the modification."
                }
            }
        } else {
            say "Sorry, the group doesn't exist.";
        }
    } else {
        say "Sorry, the user doesn't exist.";
    }
}

sub _modify_user {
    my $user  = shift @commands;
    my $login = shift @commands;
    my $email = shift @commands;

    my $s = $db->query("SELECT value FROM store WHERE key = ?", ('mypads:user:'.$user));
    if ($s->rows) {
        my $json = decode_json(encode('UTF-8', $s->hashes->first->{value}));
        say sprintf("Current login: %s", $json->{login});
        say sprintf("Current email address: %s", $json->{email});

        my $confirmed = 0;
        say sprintf("Are you sure you want to change login to %s and email address to %s? [y/N] ", ($login, $email));
        $confirmed = _parse_confirm(_get_input());
        if ($confirmed) {
            $json->{login} = $login;
            $json->{email} = $email;
            $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json), 'mypads:user:'.$user));
            say sprintf("%s updated with login %s and email address %s", ($user, $login, $email));
        } else {
            say "Aborting the modification."
        }
    } else {
        say "Sorry, the user doesn't exist.";
    }
}

sub _empty_global_author_token {
    my $user  = shift @commands;

    my $s = $db->query("SELECT value FROM store WHERE key = ?", ('mypads:user:'.$user));
    if ($s->rows) {
        my $json = decode_json(encode('UTF-8', $s->hashes->first->{value}));
        if ($json->{eplAuthorToken}) {
            my ($login, $email) = ($json->{login}, $json->{email});
            my $global_author_name;
            my $s2 = $db->query("SELECT value FROM store WHERE key = ?", ('token2author:'.$json->{eplAuthorToken}));
            if ($s2->rows) {
                my $json2 = decode_json(encode('UTF-8', $s2->hashes->first->{value}));
                my $s3 = $db->query("SELECT value FROM store WHERE key = ?", ('globalAuthor:'.$json2));
                if ($s3->rows) {
                    my $json3 = decode_json(encode('UTF-8', $s3->hashes->first->{value}));
                    $global_author_name = $json3->{name};
                }
            }
            if ($global_author_name) {
                say sprintf("Current eplAuthorToken: %s, which globalAuthor’s name is %s", ($json->{eplAuthorToken}, $global_author_name));
            } else {
                say sprintf("Current eplAuthorToken: %s (unable to find a globalAuthor for that token)", $json->{eplAuthorToken});
            }

            my $confirmed = 0;
            say sprintf("Are you sure you want to remove the eplAuthorToken (etherpad’s globalAuthor token) for user %s? (login: %s, email address: %s) [y/N] ", ($user, $login, $email));
            $confirmed = _parse_confirm(_get_input());
            if ($confirmed) {
                $json->{eplAuthorToken} = '';
                $db->query("UPDATE store SET value = ? WHERE key = ?;", (encode_json($json), 'mypads:user:'.$user));
                say sprintf("Etherpad’s globalAuthor token removed for user %s (login: %s, email address: %s)", ($user, $login, $email));
            } else {
                say "Aborting the modification."
            }
        } else {
            say "Sorry, this user don’t have a non-null eplAuthorToken.";
        }
    } else {
        say "Sorry, the user doesn't exist.";
    }
}

sub _connect {
    my $c = shift @commands;

    if (defined $c && _is_valid_base($c, 'name')) {
        $base = $c;
    } else {
        _print_bases();
        $base = _choose_base();
    }
}

sub _get_input {
    my $i = <STDIN>;
    if (defined $i) {
        chomp $i;
        return $i;
    } else {
        say "\nGoodbye :-)";
        exit;
    }
}

sub _print_bases {
    my $i = 1;
    say 'Available databases:';
    for my $base (@bases) {
        say sprintf ' %d) %s', ($i++, $base);
    }
}

sub _choose_base {
    print "\nWhich database do you want to use? ";
    my $i = _get_input();
    if (_is_valid_base($i)) {
        say sprintf 'Selected database : %s', ($bases[--$i]);
        return $bases[$i];
    } else {
        say "You didn't choose an existing database.";
        _print_bases();
        return _choose_base();
    }
}

sub _is_valid_base {
    my $i = shift;
    my $t = shift || 'number';
    if ($t eq 'name') {
        for (my $j = 0; $j < @bases; $j++) {
            return 1 if $bases[$j] eq $i;
        }
    } else {
        $i--;
        return 1 if ($i >= 0 && $i < @bases);
    }
    return 0;
}

sub _parse_confirm {
    my $confirm = shift;
    if ($confirm eq '') {
        return 0;
    } elsif ($confirm =~ m/^yes|no|y|n$/i) {
        return 1 if ($confirm =~ m/^yes|y$/i);
        return 0;
    } else {
        say 'You must write yes, y, no, or n (case-insensitive).';
        print 'Please confirm: [y/N] ';
        return _parse_confirm(_get_input());
    }
}

sub _complete_word {
    my ($text, $line, $start) = @_;
    return grep(/^$text/, @words);
}

sub _addtohistory {
    my $line = shift;
    push @history, $line;
    while (scalar(@history) > 100) {
        shift @history;
    }
}

sub _exit {
    open my $hist, '>', $history_file or die "Unable to open $history_file: $!";
    print $hist join("\n", @history);
    close $hist;
    print "\n", 'Good bye !', "\n";
    exit 0;
}
