requires "Mojo::Pg";
requires "Config::YAML";
requires "Term::ReadLine";
requires "Term::ReadLine::Gnu";
requires "Text::Slugify";
requires "File::Path";
requires "File::Spec::Functions";
