# INSTALLATION

## Dependencies

### Carton

Carton is a Perl dependencies manager, it will get what you need, so don't bother for Perl modules dependencies (but you can read the file `cpanfile` if you want).

```shell
sudo cpan Carton
```

Some modules that Carton will install need to be compiled. So you will need some tools:

```shell
sudo apt-get install build-essential libpq-dev
```

## Installation

After installing Carton:

```shell
cd /opt
git clone https://framagit.org/framasoft/pad_mynipulate.git mynipulate
cd mynipulate
carton install
sudo cp mynipulate.yml.template /etc/mynipulate.yml
sudo chmod 640 /etc/mynipulate.yml
sudo vi /etc/mynipulate.yml
```

### mynipulate

If you want to have `mynipulate` in your `$PATH`:

```shell
sudo ln -s /opt/mynipulate/mynipulate /usr/sbin/mynipulate
```

Then you can use `mynipulate` to enter interactive mode.

You can skip the base question by providing the base name on the command line:

```shell
mynipulate db_name
```
